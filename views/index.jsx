import React, { Component } from 'react';
var DefaultLayout = require('./layout/master');

class IndexComponent extends React.Component{
    render() {
        return (
            <DefaultLayout name={this.props.name}>
                <div>
                    Hello Javi
                </div>
            </DefaultLayout>
        );
      }
}

module.exports = IndexComponent;