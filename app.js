var express = require('express');
var path = require('path');

var app = express();

var routes = require('./routes/index.js');

app.use('/', routes);

app.set('port', process.env.PORT || 8080);

if (process.argv[2] ==  "dev") {
    app.use(express.static(path.join(__dirname)));
}

//View engine setup ?? 
app.set('views', __dirname + '/views');
app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine());

//Se crea una variable server en la que se indica en que puerto escucha la aplicación
var server = app.listen(app.get('port'), function () {
    console.log("app started");
});

//Conexión a la base de datos, no estará aqui en el futuro
var mysql = require('mysql');
var pool = mysql.createPool({
    connectionLimit: 10,
    host: '192.168.64.2',
    user: 'javi',
    password: 'AeccApp2016',
    database: 'pruebas'
});

pool.query('SELECT nombre FROM user WHERE id_user = ?', 2, function (error, result) {
    if (error) {
        throw error;
    } else {
        var resultado = result;
        if (resultado.length > 0) {
            console.log(resultado[0].nombre);
        } else {
            console.log('Registro no encontrado');
        }
    }
}
);
